from flask import Flask, jsonify, request
from flask_cors import CORS
import json
import requests
app = Flask(__name__)
CORS(app)


@app.route('/')
def hello_world():
    return 'Hello, World!'



@app.route('/issues')
def get_issues():

    print('Header: ', request.headers)
    headers1 = request.headers
    headers = {
        "Content-Type": "application/json",
        "Authorization": headers1['Authorization']
    }
    print("Header 2: ", headers)
    jql = request.headers['Jql']
    domain = request.headers['domain']
    print('jql', jql)
    url = "https://"+ domain+"/rest/api/2/search?jql="+jql+"&fields=summary,comment,assignee,status"
    res = requests.get(url, headers=headers)
    print(url)
    code = res.status_code
    if code != 200:
        print(res.content)
        return jsonify({
            "code": code,
            "error": json.loads(res.content)
        })
    return jsonify({
        'code': code,
        'content': json.loads(res.content)
    })
