import { TooltipOptions } from "ng2-tooltip-directive";

export const MyDefaultTooltipOptions: TooltipOptions = {
  "max-width": 750,
};
