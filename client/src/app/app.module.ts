import { MyDefaultTooltipOptions } from "./customizedTooltip";
import { AuthenticationService } from "./authentication.service";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { IssuesComponent } from "./issues/issues.component";
import { NgxPaginationModule } from "ngx-pagination";
import { TooltipModule, TooltipOptions } from "ng2-tooltip-directive";

@NgModule({
  declarations: [AppComponent, HomeComponent, IssuesComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    TooltipModule.forRoot(MyDefaultTooltipOptions as TooltipOptions),
  ],
  providers: [AuthenticationService],
  bootstrap: [AppComponent],
})
export class AppModule {}
