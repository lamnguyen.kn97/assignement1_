import { IssuesService } from "./../issues.service";
import { Component, OnInit } from "@angular/core";

class Issues {
  key: string;
  status: string;
  comments: string;
  assignee: string;
  summary: string;
  linkTo: string;
  constructor(key, status, comments, assignee, summary, linkTo) {
    this.key = key;
    this.status = status;
    this.comments = comments;
    this.assignee = assignee;
    this.summary = summary;
    this.linkTo = linkTo;
  }
}

@Component({
  selector: "app-issues",
  templateUrl: "./issues.component.html",
  styleUrls: ["./issues.component.scss"],
})
export class IssuesComponent implements OnInit {
  jql: string = "";
  p: number = 1;
  total: number;
  error: string;
  issues: Issues[];
  constructor(private issuesService: IssuesService) {}

  ngOnInit(): void {
    this.error = "";
  }
  userLoggedIn() {
    if (localStorage.getItem("oauth2-authentication")) {
      return true;
    }
    return false;
  }
  queryIssues() {
    return this.issuesService.getIssues(this.jql).subscribe((data) => {
      let code = data["code"];
      if (code == 200) {
        const issues = data["content"]["issues"];
        console.log(issues);
        const issuesMap = issues.map((value) => {
          const nComments = value["fields"]["comment"]["total"];
          if(value["fields"]["assignee"]){
            return new Issues(
              value["key"],
              value["fields"]["status"]["name"],
              value["fields"]["comment"]["comments"][nComments - 1],
              value["fields"]["assignee"]["displayName"] || "None",
              value["fields"]["summary"],
              value["self"]
            );
          }
          else{
            return new Issues(
              value["key"],
              value["fields"]["status"]["name"],
              value["fields"]["comment"]["comments"][nComments - 1],
              "None",
              value["fields"]["summary"],
              value["self"]
            );
          }
          
        });
        this.issues = issuesMap;
        if (this.error) {
          this.error = "";
        }
      } else {
        this.error = data["error"]["errorMessages"];
      }
    });
  }
}
// 0:
// expand: "operations,versionedRepresentations,editmeta,changelog,renderedFields"
// fields:
// assignee: {self: "https://lamnguyenkn97.atlassian.net/rest/api/2/user?accountId=5d36c1b79761bd0c48ee79bb", accountId: "5d36c1b79761bd0c48ee79bb", avatarUrls: {…}, displayName: "Nguyen Lam", active: true, …}
// comment: {comments: Array(3), maxResults: 3, total: 3, startAt: 0}
// status: {self: "https://lamnguyenkn97.atlassian.net/rest/api/2/status/10000", description: "", iconUrl: "https://lamnguyenkn97.atlassian.net/", name: "To Do", id: "10000", …}
// summary: "List the available jira issues with jira API"
// __proto__: Object
// id: "10001"
// key: "LOG-2"
// self: "https://lamnguyenkn97.atlassian.net/rest/api/2/issue/10001"
