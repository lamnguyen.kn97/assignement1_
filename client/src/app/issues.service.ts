import { HttpRequest, HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Config } from "../environments/api_config";

@Injectable({
  providedIn: "root",
})
export class IssuesService {
  constructor(private http: HttpClient) {}

  getIssues(jql) {
    let config = new Config();
    const domain = localStorage.getItem("domain");
    const email = config.GP_EMAIL;
    const token = config.GP_API_TOKEN;
    const encoded_string = btoa(`${email}:${token}`);
    console.log(encoded_string);
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Basic ${encoded_string}`,
        JQL: jql,
        domain: domain
      }),
    };

    return this.http.get(config.API_URL + "/issues", httpOptions);
  }
}
