import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpResponse
} from "@angular/common/http";
import { Config } from "../environments/api_config";
import { Observable, pipe, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { HttpHeaders } from "@angular/common/http";

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient) {}
  private handleError(error) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError("Something bad happened; please try again later.");
  }
  getUserJira(email_){
    return ;

  }
  getUserInfo() {
    var params = JSON.parse(localStorage.getItem("oauth2-authentication"));
    if (params && params["access_token"]) {
      const url: string =
        "https://www.googleapis.com/drive/v3/about?fields=user&" +
        "access_token=" +
        params["access_token"];
      return this.http.get(url);
    }
  }
  get_authorization_request() {
    const cfg: Config = new Config();
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     "Access-Control-Allow-Origin": "*"
    //   })
    // };
    // let headers: HttpHeaders = new HttpHeaders();
    // headers = headers.append("Accept", "application/json");
    // headers = headers.append("zumo-api-version", "2.0.0");
    // return this.http
    //   .get(cfg.API_URL + "/login_with_Google/", {
    //     headers: headers
    //   })
    //   .pipe(catchError(this.handleError));
    var oauth2Endpoint = "https://accounts.google.com/o/oauth2/v2/auth";

    // Create <form> element to submit parameters to OAuth 2.0 endpoint.
    var form = document.createElement("form");
    form.setAttribute("method", "GET"); // Send as a GET request.
    form.setAttribute("action", oauth2Endpoint);

    // Parameters to pass to OAuth 2.0 endpoint.
    var params = {
      client_id: cfg.CLIENT_ID,
      redirect_uri: cfg.REDIRECT_URL,
      response_type: "token",
      scope: "https://www.googleapis.com/auth/drive.metadata.readonly",
      include_granted_scopes: "true",
      state: "pass-through value"
    };

    // Add form parameters as hidden input values.
    for (var p in params) {
      var input = document.createElement("input");
      input.setAttribute("type", "hidden");
      input.setAttribute("name", p);
      input.setAttribute("value", params[p]);
      form.appendChild(input);
    }

    // Add form to page and submit it to open the OAuth 2.0 endpoint.
    document.body.appendChild(form);
    form.submit();
  }
}
