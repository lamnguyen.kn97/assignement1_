import { AuthenticationService } from "./../authentication.service";
import { Component, OnInit } from "@angular/core";
import { Config } from "../../environments/api_config";
import { User } from "../User";
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  constructor(private authenticationService: AuthenticationService) {}
  user: User;
  loggedIn: Boolean = false;
  defaultURL: string = "id.alatssian.com"
  submitted = false;
  emailJira;
  nameJira;
  onSubmit() { this.submitted = true; }
  ngOnInit(): void {
    var user_loggin = this.checkLocationHash();
    if (user_loggin) {
      this.loggedIn = true;
      this.queryUserInfo();
    }
  }
  loginWithGoogle() {
    localStorage.setItem('domain',this.defaultURL);
    return this.authenticationService.get_authorization_request();
  }
  queryUserInfo() {
    return this.authenticationService.getUserInfo().subscribe(data => {
      var user = data["user"];
      var newUser = new User(user["displayName"], user["emailAddress"]);
      this.user = newUser;
     
    });
  }
 
  checkLocationHash() {
    var fragmentString = location.hash.substring(1);

    // Parse query string to see if page request is coming from OAuth 2.0 server.
    var params = {};
    var regex = /([^&=]+)=([^&]*)/g,
      m;
    while ((m = regex.exec(fragmentString))) {
      params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
    }
    if (Object.keys(params).length > 0) {
      localStorage.setItem("oauth2-authentication", JSON.stringify(params));
      return true;
    }
    return false;
  }
}
